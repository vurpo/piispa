#include <Arduino.h>
#include <Wire.h>

//#define SERIAL_DEBUG

const uint8_t motor_a_en = 4;
const uint8_t motor_a_1  = 3;
const uint8_t motor_a_2  = 5;

const uint8_t motor_b_en = 7;
const uint8_t motor_b_1  = 6;
const uint8_t motor_b_2  = 9;

enum MotorStatus{
  coast = 0,
  forward = 1,
  backward = 2,
  brake = 3,
};

volatile uint8_t new_recv_values[3] = {0, 0, 0};
uint8_t recv_values[3] = {0, 0, 0};

MotorStatus motor_status_a = coast;
MotorStatus motor_status_b = coast;

void receiveEvent(int16_t c);

void setup() {
  pinMode(motor_a_en, OUTPUT);
  pinMode(motor_a_1 , OUTPUT);
  pinMode(motor_a_2 , OUTPUT);

  pinMode(motor_b_en, OUTPUT);
  pinMode(motor_b_1 , OUTPUT);
  pinMode(motor_b_2 , OUTPUT);

  digitalWrite(motor_a_en, HIGH);
  digitalWrite(motor_b_en, HIGH);

  Wire.begin(0x43);
  Wire.onReceive(receiveEvent);
  digitalWrite(SCL, LOW);
  digitalWrite(SDA, LOW);

  #ifdef SERIAL_DEBUG
  Serial.debug(9600);
  #endif
}

void loop() {
  noInterrupts();
    memcpy(recv_values, (void*)new_recv_values, sizeof(new_recv_values));
  interrupts();

  motor_status_a = static_cast<MotorStatus>(recv_values[2]&0xF);
  motor_status_b = static_cast<MotorStatus>((recv_values[2]&0xF0)>>4);

  if (motor_status_a&1) {
    analogWrite(motor_a_1, recv_values[0]);
  } else {
    analogWrite(motor_a_1, 0);
  }
  if (motor_status_a&2) {
    analogWrite(motor_a_2, recv_values[0]);
  } else {
    analogWrite(motor_a_2, 0);
  }

  if (motor_status_b&1) {
    analogWrite(motor_b_1, recv_values[1]);
  } else {
    analogWrite(motor_b_1, 0);
  }
  if (motor_status_b&2) {
    analogWrite(motor_b_2, recv_values[1]);
  } else {
    analogWrite(motor_b_2, 0);
  }

}

void receiveEvent(int16_t c) {
  for (int i = 0; i<c; i++) {
    new_recv_values[i] = Wire.read();
  }
}
