#include <Arduino.h>
#include <Wire.h>
#include <FastLED.h>

//#define SERIAL_DEBUG

#define NUM_LEDS 24
#define DATA_PIN 2

CRGB leds[NUM_LEDS];
volatile bool booting = true;

void receiveEvent(int16_t c);

void setup() {
  FastLED.addLeds<WS2812B, DATA_PIN, GRB>((CRGB*)leds, NUM_LEDS);

  Wire.begin(0x44);
  Wire.onReceive(receiveEvent);
  digitalWrite(SCL, LOW);
  digitalWrite(SDA, LOW);

  #ifdef SERIAL_DEBUG
  Serial.debug(9600);
  #endif
}

void loop() {
  if (booting) { //this block is just the booting animation
    static uint8_t current_pixel = 0;
    for (int i=0; i<NUM_LEDS; i++) {
      leds[i] = CRGB((i==current_pixel||i==(((NUM_LEDS-6-current_pixel)%NUM_LEDS+NUM_LEDS)%NUM_LEDS))?255:0, 0, 0);
    }
    FastLED.show();
    current_pixel++;
    current_pixel %= NUM_LEDS;
    delay(60);
  } else {
    FastLED.show();
  }

}

void receiveEvent(int16_t c) {
  booting = false;
  int i = Wire.read();
  if (i>=0 && i<NUM_LEDS) {
    leds[i] = CRGB(Wire.read(), Wire.read(), Wire.read());
  }
  while (Wire.available()) {
    Wire.read();
  }
}
