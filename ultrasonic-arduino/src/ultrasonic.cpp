#include <Arduino.h>
#include <Wire.h>
#include <NewPing.h>

//#define SERIAL_DEBUG

NewPing forward(2,3,200);
NewPing left(9,8,200);
NewPing right(4,5,200);

int16_t values[3];
volatile int16_t current_values[3];

void requestEvent();

void pingEvent() {
  current_values[0] = forward.ping();
  current_values[1] = left.ping();
  current_values[2] = right.ping();
}

void setup() {
  digitalWrite(A4, LOW);
  digitalWrite(A5, LOW);
  Wire.begin(0x42);
  Wire.onRequest(requestEvent);
  //digitalWrite(A4, LOW);
  //digitalWrite(A5, LOW);

  //NewPing::timer_ms(10, pingEvent);

  #ifdef SERIAL_DEBUG
  Serial.begin(9600);
  #endif
}

void loop() {
  noInterrupts();
    memcpy(values, (void*)current_values, sizeof(int16_t)*3);
  interrupts();

  //delay(10);
  #ifdef SERIAL_DEBUG
  Serial.print(values[0]);
  Serial.print(" ");
  Serial.print(values[1]);
  Serial.print(" ");
  Serial.println(values[2]);
  #endif
}

void requestEvent() {
  Wire.write((uint8_t*)values, 6);
}
