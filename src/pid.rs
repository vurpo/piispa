use num_traits::float::FloatConst;

pub struct PIDController {
  p_gain: f64,
  i_gain: f64,
  d_gain: f64,

  error_prior: f64,
  integral: f64,

  target_value: f64,

  circular_range: Option<(f64,f64)>,
}

impl PIDController {
  pub fn new(p_gain: f64, i_gain: f64, d_gain: f64, circular_range: Option<(f64,f64)>) -> PIDController {
    PIDController {
      p_gain: p_gain,
      i_gain: i_gain,
      d_gain: d_gain,

      error_prior: 0.0,
      integral: 0.0,

      target_value: 0.0,

      circular_range: circular_range,
    }
  }

  pub fn set_target_value(&mut self, target_value: f64) {
    self.target_value = target_value;
  }

  pub fn update(&mut self, delta_time: f64, reading: f64) -> f64 {
    let mut error = self.target_value - reading;
    if let Some(range) = self.circular_range {
      if error > f64::PI() {
        //error -= (f64::PI()*2.0);
      }
    }

    self.integral = self.integral + (error*delta_time);
    let derivative = (error - self.error_prior)/delta_time;
    let output = self.p_gain*error + self.i_gain*self.integral + self.d_gain*derivative;
    self.error_prior = error;

    output
  }
}
