use std::{
  time,
  thread,
  env,
};
use std::sync::mpsc;
use i2c_imu::{
  ADXL345,
  HMC5883L,
  ITG3200,
};
use i2c_distance::*;
use i2c_motor::*;
use i2c_led::*;
use pid::*;
use num_traits::float::FloatConst;
use websocket_server;

use time::*;
use nalgebra::{
  Vector3, UnitQuaternion, Quaternion,
  RotationTo,
};
use madgwick_ahrs::MadgwickAHRS;

fn xy_to_tank_controls(x: f32, y: f32) -> (f32, f32) {
  /*let v = (1.0-x.abs()) * y + y;
  let w = (1.0-y.abs()) * x + x;
  let r = (v+w)/2.0;
  let l = (v-w)/2.0;
  
  (l,r)*/
  (y-x.max(-1.0).min(1.0), y+x.max(-1.0).min(1.0)) // This is a magic function that converts joystick (X, Y) movement to tank controls
}

fn heading_from_quaternion(quaternion: &Quaternion<f32>, relative_angle: f32) -> f32 {
  let REF_QUATERNION: UnitQuaternion<f32> = UnitQuaternion::from_scaled_axis(Vector3::new(0.0, 0.0, relative_angle));

  let ahrs: UnitQuaternion<f32> = UnitQuaternion::from_quaternion(quaternion);
  let angle_north = {
    let rotation = REF_QUATERNION.rotation_to(&ahrs);
    let mut angle = f32::atan2(rotation.as_ref().k, rotation.as_ref().w)*-2.0;
    if angle >  f32::PI() { angle -= f32::PI()*2.0 }
    if angle < -f32::PI() { angle += f32::PI()*2.0 }
    angle
  };

  angle_north
}

fn wrap(x: f64, min: f64, max: f64) -> f64 {
  ((x-min)%(max-min))+min
}

#[derive(Debug,RustcDecodable)]
pub enum RobotCommand {
  Stop,
  SwitchToManual,
  SetAutonomous{target_index: i32},
  JoystickControl{x: f64, y: f64, timeout: f64},
  SetHeadingDirection{index: i32, value: f64},
  //TODO: implement all commands
}

pub enum RobotState {
  Starting, // This is to let the AHRS settle before we take any measurements from it
  Running,
  ManualControl{x: f64, y: f64},
  Autonomous(AutonomousState),
  Idle,
}

pub enum Direction {
  Left,
  Right,
}
pub enum AutonomousState {
  Forward{avg_heading: f32, avg_count: i32},
  Turning{last_yaw: f32},
}

pub struct Robot {
  // Sensors
  ultrasonic: PiispaUltrasonic,
  gyro: ITG3200,
  magnetometer: HMC5883L,
  accelerometer: ADXL345,

  led: PiispaLed,

  // Readings
  pub distances: UltrasonicReading,
  pub attitude_heading: MadgwickAHRS,
  pub rotation: Quaternion<f32>,

  // State
  pub yaw: f32,
  pub target_heading: f32,
  motor_turning_pid: PIDController,
  last_update: PreciseTime,
  start_time: PreciseTime,
  heading_directions: [f32; 4],

  state_tx: mpsc::Sender<websocket_server::RobotStateUpdate>,

  pub command_tx: mpsc::Sender<RobotCommand>,
  command_rx: mpsc::Receiver<RobotCommand>,

  command_timeout: f64,

  pub current_state: RobotState,

  // Output
  motor_controller: PiispaMotor,
  
}

impl Robot {
  pub fn new() -> Result<(Robot, mpsc::Receiver<websocket_server::RobotStateUpdate>),()> {
    let args: Vec<String> = env::args().collect();
    let p_gain: f64 = args[1].parse().unwrap();
    let i_gain: f64 = args[2].parse().unwrap();
    let d_gain: f64 = args[3].parse().unwrap();

    let mut ultrasonic = try!(PiispaUltrasonic::new());
    let gyro = try!(ITG3200::new());
    let magnetometer = try!(HMC5883L::with_calibration());
    let accelerometer = try!(ADXL345::new());
    let led = try!(PiispaLed::new());

    let distances = try!(ultrasonic.get_distances_m());
    let attitude_heading = MadgwickAHRS::new(0.1, 5.0);
    let rotation = attitude_heading.quaternion.clone();

    let target_heading = f32::PI();
    let mut motor_turning_pid = PIDController::new(p_gain, i_gain, d_gain, Some((-f64::PI(), f64::PI())));
    motor_turning_pid.set_target_value(0.0);//target_heading as f64);
    let last_update = PreciseTime::now();
    let start_time = PreciseTime::now();

    let (tx, rx) = mpsc::channel();
    let (state_tx, state_rx) = mpsc::channel();

    let motor_controller = try!(PiispaMotor::new());
    
    println!("Starting robot! Let the AHRS settle for a couple of seconds first.");

    Ok(( Robot {
      ultrasonic: ultrasonic,
      gyro: gyro,
      magnetometer: magnetometer,
      accelerometer: accelerometer,
      
      led: led,

      distances: distances,
      attitude_heading: attitude_heading,
      rotation: rotation,

      target_heading: target_heading,
      yaw: 0.0,
      motor_turning_pid: motor_turning_pid,
      last_update: last_update,
      start_time: start_time,
      heading_directions: [0.0, 0.0, 0.0, 0.0],

      state_tx: state_tx,

      command_tx: tx,
      command_rx: rx,

      command_timeout: 0.0,

      current_state: RobotState::Starting,

      motor_controller: motor_controller,
    }, state_rx))
  }

  pub fn update(&mut self) {

    let acc_out = self.accelerometer.get_axes_g().unwrap();
    let mag_out = self.magnetometer.get_axes_raw().unwrap();
    let gyro_out = self.gyro.get_axes_rad().unwrap();

    self.attitude_heading.update_gyro_acc_mag(gyro_out, acc_out, Vector3::new(mag_out[0] as f32, mag_out[1] as f32, mag_out[2] as f32));
    self.rotation.clone_from(&self.attitude_heading.quaternion);
    self.yaw = heading_from_quaternion(&self.rotation, -self.target_heading);
    let absolute_yaw = heading_from_quaternion(&self.rotation, 0.0);

    self.distances = self.ultrasonic.get_distances_m().unwrap();

    while let Ok(command) = self.command_rx.try_recv() {
      match command {
        RobotCommand::Stop => {
          self.current_state = RobotState::Idle;
          self.motor_controller.set_both_motors(0.0, 0.0);
        },
        RobotCommand::SwitchToManual => {
          println!("Switch to manual control");
          self.current_state = RobotState::ManualControl{x: 0.0, y: 0.0};
        },
        RobotCommand::JoystickControl{x,y,timeout} => {
          self.command_timeout = timeout;
          self.current_state = RobotState::ManualControl{x: x, y: y};
        },
        RobotCommand::SetHeadingDirection{index,value} => {
          if let Some(x) = self.heading_directions.get_mut(index as usize) {
            *x = (value as f32);
          }
          println!("directions is now {:?}", self.heading_directions);
        },
        RobotCommand::SetAutonomous{target_index} => {
          if let Some(x) = self.heading_directions.get(target_index as usize) {
            self.target_heading = *x;
            self.current_state = RobotState::Autonomous(AutonomousState::Forward{avg_heading: absolute_yaw, avg_count: 1});
          }
        },
      }
    }

    match self.current_state {
      RobotState::Starting => {
        // Do nothing, until...
        if self.start_time.to(PreciseTime::now()).num_seconds() >= 10 {
          println!("Should be done settling now...");
          //thread::spawn(move || {

          self.current_state = RobotState::Idle;
        }
      },

      RobotState::Running => {
        if self.start_time.to(PreciseTime::now()).num_seconds() == 4 {
          println!("Changing angle!");
          self.target_heading = f32::PI()/2.0;
        }
        if self.start_time.to(PreciseTime::now()).num_seconds() == 8 {
          println!("Changing angle!");
          self.target_heading = 0.0;
        }
        if self.start_time.to(PreciseTime::now()).num_seconds() == 12 {
          println!("Changing angle!");
          self.target_heading = f32::PI()/-2.0;
        }
        if self.start_time.to(PreciseTime::now()).num_seconds() == 16 {
          println!("Changing angle!");
          self.target_heading = f32::PI();
        }

        let delta_time = self.last_update.to(PreciseTime::now()).num_milliseconds() as f64/1000.0;
        self.last_update = PreciseTime::now();

        let turning_amount = self.motor_turning_pid.update(delta_time, self.yaw as f64);
        //println!("{}", turning_amount);

        let tank_control = xy_to_tank_controls(turning_amount as f32, -1.0);
        self.motor_controller.set_both_motors(-tank_control.0, -tank_control.1);
      },

      RobotState::ManualControl{x,y} => {
        let delta_time = self.last_update.to(PreciseTime::now()).num_milliseconds() as f64/1000.0;
        self.last_update = PreciseTime::now();

        self.command_timeout -= delta_time;
        let (tank_left, tank_right) = xy_to_tank_controls(x as f32, y as f32);
        self.motor_controller.set_both_motors(tank_left, tank_right);

        if self.command_timeout <= 0.0 {
          self.current_state = RobotState::Idle;
          self.motor_controller.set_both_motors(0.0, 0.0);
          println!("Timeout for manual command reached, setting to idle!");
        };
      },

      RobotState::Autonomous(ref mut autonomous_state) => {
        let delta_time = self.last_update.to(PreciseTime::now()).num_milliseconds() as f64/1000.0;
        self.last_update = PreciseTime::now();

        let (forward_speed, turning_amount) = match *autonomous_state {
          AutonomousState::Forward{avg_heading, avg_count} => {
            *autonomous_state = AutonomousState::Forward{avg_heading: (avg_heading*(avg_count as f32)+absolute_yaw)/((avg_count+1) as f32), avg_count: avg_count+1};
            //let turning_amount = self.motor_turning_pid.update(delta_time, self.yaw as f64);
            let wall_differences = {
              if self.distances.left < 1.0 && self.distances.right < 1.0 {
                self.distances.right-self.distances.left
              } else {
                0.0
              }
            };
            if self.distances.forward < 0.4 {
              *autonomous_state = AutonomousState::Turning{last_yaw: self.yaw};
              if self.distances.left > self.distances.right {
                self.target_heading = wrap(absolute_yaw as f64 - f64::PI()/2.0, -f64::PI(), f64::PI()) as f32;
              } else {
                self.target_heading = wrap(absolute_yaw as f64 + f64::PI()/2.0, -f64::PI(), f64::PI()) as f32;
              }
            }
            (1.0, wall_differences*1.2)
          },
          AutonomousState::Turning{last_yaw} => {
            let turning_amount = self.motor_turning_pid.update(delta_time, self.yaw as f64);
            if (last_yaw < 0.0) != (self.yaw < 0.0) { // if last_yaw and self.yaw are on opposite sides of 0.0
              *autonomous_state = AutonomousState::Forward{avg_heading: absolute_yaw, avg_count: 1};
            } else {
              *autonomous_state = AutonomousState::Turning{last_yaw: self.yaw};
            }
            (0.5, turning_amount)
          },
        };

        let tank_control = xy_to_tank_controls(-turning_amount as f32, forward_speed);
        self.motor_controller.set_both_motors(tank_control.0, tank_control.1);
      },
      
      RobotState::Idle => {
        ()
      },
    };

    for i in 10..21 {
      self.led.set_led(i, (((self.distances.left*0.8).min(1.0).max(0.0)*255.0) as u8, 0, 0));
    }
    for i in 0..9 {
      self.led.set_led(i, (0, ((self.distances.right*0.8).min(1.0).max(0.0)*255.0) as u8, 0));
    }
    for i in 22..24 {
      self.led.set_led(i, (0, ((self.distances.right*0.8).min(1.0).max(0.0)*255.0) as u8, 0));
    }

    self.state_tx.send(websocket_server::RobotStateUpdate::Heading(absolute_yaw as f64));
    self.state_tx.send(websocket_server::RobotStateUpdate::DistanceForward(self.distances.forward as f64));
    self.state_tx.send(websocket_server::RobotStateUpdate::DistanceLeft(self.distances.left as f64));
    self.state_tx.send(websocket_server::RobotStateUpdate::DistanceRight(self.distances.right as f64));
  }
}
