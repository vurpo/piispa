use i2cdev::linux::LinuxI2CDevice;
use i2cdev::core::I2CDevice;
use std::cmp::{min,max};

const I2C_ADDRESS: u8 = 0x43;

const MOTOR_DEAD_ZONE: f32 = 0.34; // motors don't have enough torque to move below this speed

pub struct PiispaMotor {
  bus: LinuxI2CDevice,
  address: u8,
  motor_values: [i16; 2],
  motor_status: [MotorStatus; 2],
}

#[derive(Copy, Clone)]
pub enum Motor {
  Left = 0,
  Right = 1,
}

#[derive(Copy, Clone)]
pub enum MotorStatus {
  Coast = 0,
  Forward = 1,
  Backward = 2,
  Brake = 3,
}

impl PiispaMotor {
  pub fn new() -> Result<PiispaMotor, ()> {
    let bus = try!(LinuxI2CDevice::new("/dev/i2c-1", I2C_ADDRESS as u16).map_err(|_|()));
    let motorcontroller = PiispaMotor { bus: bus, address: I2C_ADDRESS, motor_values: [0,0], motor_status: [MotorStatus::Coast, MotorStatus::Coast] };
    Ok(motorcontroller)
  }

  pub fn set_motor(&mut self, motor: Motor, motor_status: MotorStatus, speed: i16) -> Result<(),()> {
    self.motor_values[motor as usize] = max(-255, min(255, speed));
    self.motor_status[motor as usize] = motor_status;
    let mut  motor_flags: u8 = 0;
    motor_flags |= (self.motor_status[0] as u8)<<4;
    motor_flags |= self.motor_status[1] as u8;

    try!(self.bus.write(&[max(0, min(255, self.motor_values[0].abs())) as u8, max(0, min(255, self.motor_values[1].abs())) as u8, motor_flags]).map_err(|_|()));

    Ok(())
  }

  pub fn set_both_motors(&mut self, left_speed: f32, right_speed: f32) -> Result<(), ()> {
    let (mut motor_left, mut motor_right) = ((left_speed.max(-1.0).min(1.0) * 255.0) as i16, (right_speed.max(-1.0).min(1.0) * 255.0) as i16);
    if left_speed.abs() < MOTOR_DEAD_ZONE { motor_left = 0; }   // dead zone
    if right_speed.abs() < MOTOR_DEAD_ZONE { motor_right = 0; }

    self.motor_values[Motor::Left as usize] = max(-255, min(255, motor_left));
    self.motor_values[Motor::Right as usize] = max(-255, min(255, motor_right));

    let mut motor_flags: u8 = 0;
    motor_flags |= (motor_right.is_positive() as u8 + 1)<<4;
    motor_flags |= motor_left.is_positive() as u8 + 1;

    try!(self.bus.write(&[max(0, min(255, self.motor_values[Motor::Left as usize].abs())) as u8, max(0, min(255, self.motor_values[Motor::Right as usize].abs())) as u8, motor_flags]).map_err(|_|()));

    Ok(())
  }
}
