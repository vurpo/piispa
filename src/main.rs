extern crate i2cdev;
extern crate i2c_imu;
extern crate madgwick_ahrs;
extern crate nalgebra;
extern crate time;
extern crate num_traits;
extern crate ws;
extern crate bus;
extern crate rustc_serialize;
extern crate iron;

use std::thread;

pub mod i2c_distance;
mod i2c_motor;
mod pid;
pub mod robot;
pub mod websocket_server;
pub mod i2c_led;

fn main() {
  let (mut robot, state_rx) = robot::Robot::new().unwrap();
  let command_tx = robot.command_tx.clone();
  let (state_tx, command_rx) = websocket_server::start_websocket_server();

  thread::spawn(move || {
    let state_tx = state_tx.clone();
    loop {
      let recv = state_rx.recv().unwrap();
      state_tx.send(recv);
    }
  });
  thread::spawn(move || {
    let command_tx = command_tx.clone();
    loop {
      let recv = command_rx.recv().unwrap();
      command_tx.send(recv);
    }
  });

  loop {
    robot.update();
    thread::sleep(std::time::Duration::from_millis(100));
  }
}
