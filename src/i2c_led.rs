use i2cdev::linux::LinuxI2CDevice;
use i2cdev::core::I2CDevice;

const I2C_ADDRESS: u8 = 0x44;

pub struct PiispaLed {
  bus: LinuxI2CDevice,
  address: u8
}

impl PiispaLed {
  pub fn new() -> Result<PiispaLed, ()> {
    let bus = try!(LinuxI2CDevice::new("/dev/i2c-1", I2C_ADDRESS as u16).map_err(|_|()));
    let sensor = PiispaLed { bus: bus, address: I2C_ADDRESS };
    Ok(sensor)
  }

  pub fn set_led(&mut self, index: u8, color: (u8, u8, u8)) -> Result<(), ()> {
    Ok(try!(self.bus.write(&[index, color.2, color.1, color.0]).map_err(|_|())))
  }
}
