use ws;
use std::thread;
use std::sync::mpsc;
use std::sync::{
  Arc, Mutex,
};
use bus;
use rustc_serialize::json;
use iron::prelude::*;
use iron::status;
use iron::headers::ContentType;

use robot::RobotCommand;
use i2c_distance::UltrasonicReading;

#[derive(Clone, RustcEncodable)]
pub enum RobotStateUpdate {
  DistanceForward(f64),
  DistanceLeft(f64),
  DistanceRight(f64),
  Heading(f64),
  //TODO: more readings to come?
}

struct Server {
  close_tx: mpsc::Sender<()>,
  out: ws::Sender,
  cmd_tx: mpsc::Sender<RobotCommand>,
}

impl ws::Handler for Server {
  fn on_message(&mut self, msg: ws::Message) -> Result<(),ws::Error> {
    let text = try!(msg.into_text());
    match json::decode::<RobotCommand>(&text) {
      Ok(message) => {
        self.cmd_tx.send(message);
      },
      Err(_) => {
        println!("malformed JSON message received: {}", text);
        //TODO
      },
    }

    Ok(())
  }

  fn on_close(&mut self, _: ws::CloseCode, _: &str) {
    println!("connection closed");
    self.close_tx.send(());
  }
}

pub fn start_websocket_server() -> (mpsc::Sender<RobotStateUpdate>, mpsc::Receiver<RobotCommand>) {
  let (state_tx, state_rx) = mpsc::channel();              //robot state channel (robot to client)
  let state_bus = Arc::new(Mutex::new(bus::Bus::new(50))); //god fucking damnit, there's a mutex in the code
  let (cmd_tx, cmd_rx) = mpsc::channel();                  //command channel (client to robot)
  println!("start listening");
  thread::Builder::new().name("websocket server".to_owned()).spawn(move || {
    let state_bus_send = state_bus.clone();
    thread::spawn(move || {
      loop {
        let recv = state_rx.recv().unwrap();
        state_bus_send.lock().unwrap().broadcast(recv); //IF THE SERVER STOPS, THIS IS THE PROBLEM (see https://github.com/jonhoo/bus/issues/6)
      }
    });
    println!("{:?}", ws::listen("0.0.0.0:3012", |out| {
      let (mut rx, tx) = (state_bus.lock().unwrap().add_rx(), cmd_tx.clone());
      let socket_sender = out.clone();
      let (close_tx, close_rx) = mpsc::channel();
      let thread = thread::Builder::new().name("socket".to_owned()).spawn(move || {
        'main: loop {
          if let Ok(_) = close_rx.try_recv() {
            break 'main;
          }
          match rx.recv() {
            Ok(recv) => {
              match json::encode(&recv) {
                Ok(data) => {
                  if let Err(e) = socket_sender.send(data) {
                    println!("error: {:?}", e);
                    break 'main;
                  }
                },
                Err(e) => {
                  println!("error while encoding JSON: {:?}", e);
                },
              }
            }
            Err(_) => {
              break 'main;
            }
          }
        }
        println!("socket closed");
      });
      println!("new socket!");

      Server {
        close_tx: close_tx,
        out: out,
        cmd_tx: tx.clone(),
      }
    }));
    println!("uh oh server ended");
  });

  println!("starting webserver");
  thread::Builder::new().name("webserver".to_owned()).spawn(move || {
    Iron::new(|request: &mut Request| {
      Ok(Response::with((ContentType::html().0, status::Ok, include_str!("../robot-webui/index.html"))))
    }).http("0.0.0.0:1337").unwrap();
    println!("webserver ended");
  });
  println!("started webserver");

  (state_tx, cmd_rx)
}
