use i2cdev::linux::LinuxI2CDevice;
use i2cdev::core::I2CDevice;

const I2C_ADDRESS: u8 = 0x42;

const SOUND_SPEED_M_US: f64 = 0.0003434; // Speed of sound in air as meters/microsecond (multiply raw reading with this)

#[derive(Clone, RustcEncodable)]
pub struct UltrasonicReading {
  pub forward: f64,
  pub left: f64,
  pub right: f64,
}

pub struct PiispaUltrasonic {
  bus: LinuxI2CDevice,
  address: u8
}

impl PiispaUltrasonic {
  pub fn new() -> Result<PiispaUltrasonic, ()> {
    let bus = try!(LinuxI2CDevice::new("/dev/i2c-1", I2C_ADDRESS as u16).map_err(|_|()));
    let sensor = PiispaUltrasonic { bus: bus, address: I2C_ADDRESS };
    Ok(sensor)
  }

  pub fn get_distances_raw(&mut self) -> Result<[u16; 3], ()> {
    let mut buffer: [u8; 6] = [0; 6];
    try!(self.bus.read(&mut buffer).map_err(|_|()));

    Ok([(buffer[1] as u16)<<8|buffer[0] as u16, (buffer[3] as u16)<<8|buffer[2] as u16, (buffer[5] as u16)<<8|buffer[4] as u16])
  }

  pub fn get_distances_m(&mut self) -> Result<UltrasonicReading, ()> {
    let raw_readings = try!(self.get_distances_raw());
    Ok(UltrasonicReading{
      forward: raw_readings[0] as f64*SOUND_SPEED_M_US,
      left: raw_readings[1] as f64*SOUND_SPEED_M_US,
      right: raw_readings[2] as f64*SOUND_SPEED_M_US,
    })
  }
}
