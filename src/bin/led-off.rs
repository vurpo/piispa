extern crate i2cdev;

use i2cdev::linux::LinuxI2CDevice;
use i2cdev::core::I2CDevice;

use std::thread;

const I2C_ADDRESS: u8 = 0x44;

fn main() {
  let mut bus = LinuxI2CDevice::new("/dev/i2c-1", I2C_ADDRESS as u16).unwrap();
  
  for x in 0..41 {
    for i in 0..25 {
      bus.write(&[i,0,0,((x as f32/12.732).sin()*200.0) as u8]);
    }
    //thread::sleep_ms(5);
  }
}
