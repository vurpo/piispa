extern crate i2c_imu;
extern crate madgwick_ahrs;
extern crate nalgebra;

use i2c_imu::{
  ADXL345,
  ITG3200,
  HMC5883L,
};
use madgwick_ahrs::MadgwickAHRS;
use nalgebra::{
  Vector3,
};
use std::{ time, thread };

fn main() {
  let mut acc = ADXL345::new().unwrap();
  let mut mag = HMC5883L::with_calibration().unwrap();
  let mut gyro = ITG3200::new().unwrap();

  let mut ahrs = MadgwickAHRS::new(0.016, 1.5);

  loop {
    let acc_out = acc.get_axes_g().unwrap();
    let mag_out = mag.get_axes_raw().unwrap();
    let gyro_out = gyro.get_axes_rad().unwrap();

    ahrs.update_gyro_acc_mag(gyro_out, acc_out, Vector3::new(mag_out[0] as f32, mag_out[1] as f32, mag_out[2] as f32));

    println!("{} {} {} {}", ahrs.quaternion.w, ahrs.quaternion.i, ahrs.quaternion.j, ahrs.quaternion.k);
    thread::sleep(time::Duration::from_millis(16));
  }
}
