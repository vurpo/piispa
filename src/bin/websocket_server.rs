extern crate piispa;
extern crate rand;

use std::thread;
use piispa::{
  websocket_server,
  i2c_distance,
};

fn main() {
  println!("starting websocket server");
  let (tx, rx) = websocket_server::start_websocket_server();
  println!("websocket server started");

  thread::spawn(move || {
    loop {
      let recv = rx.recv();
      println!("Received command: {:?}", recv);
    }
  });

  loop {
    tx.send(websocket_server::RobotStateUpdate::DistanceForward(rand::random()));
    thread::sleep_ms(200);
    tx.send(websocket_server::RobotStateUpdate::DistanceLeft(rand::random()));
    thread::sleep_ms(200);
    tx.send(websocket_server::RobotStateUpdate::DistanceRight(rand::random()));
    thread::sleep_ms(200);
    tx.send(websocket_server::RobotStateUpdate::Heading(rand::random()));
    thread::sleep_ms(200);
  }
}
