extern crate i2c_imu;
extern crate madgwick_ahrs;
extern crate nalgebra;

use i2c_imu::HMC5883L;
use std::{ time, thread };

fn main() {
  let mut mag = HMC5883L::new().unwrap();

  loop {
    let mag_out = mag.get_axes_raw().unwrap();

    println!("{} {} {}", mag_out[0] as f32/250.0, mag_out[1] as f32/250.0, mag_out[2] as f32/250.0);
    thread::sleep(time::Duration::from_millis(16));
  }
}
